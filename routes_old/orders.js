const express = require("express");
const mysql = require("mysql2");
const router = express.Router();
require("dotenv").config();
var con = require("../db");

//Get
router.get("/", (req, res) => {
  var sql = "select * from orders";
  con.query(sql, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//Get by ID
router.get("/:id", (req, res) => {
  const sql = "select * from orders where order_id = ?";
  con.query(sql, [req.params.id], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});
//POST - ADD order
router.post("/", (req, res) => {
  const quiqupJobID = req.body.quiqupJobID;
  const pickupState = req.body.pickupState;
  const dropOffstate = req.body.dropOffstate;
  const pickupTrackingURL = req.body.pickupTrackingURL;
  const dropOffTrackingURL = req.body.dropOffTrackingURL;
  const data = [
    quiqupJobID,
    pickupState,
    dropOffstate,
    pickupTrackingURL,
    dropOffTrackingURL,
  ];
  var sql =
    "insert into orders (quiqupJobID, pickupState, dropOffstate, pickupTrackingURL, dropOffTrackingURL) values(?,?,?,?,?)";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//PUT - UPDATE BANNER
router.put("/:id", (req, res) => {
  const quiqupJobID = req.body.quiqupJobID;
  const pickupState = req.body.pickupState;
  const order_id = req.params.id;
  const dropOffstate = req.body.dropOffstate;
  const pickupTrackingURL = req.body.pickupTrackingURL;
  const dropOffTrackingURL = req.body.dropOffTrackingURL;
  const data = [
    quiqupJobID,
    pickupState,
    dropOffstate,
    pickupTrackingURL,
    dropOffTrackingURL,
    order_id,
  ];
  var sql =
    "update orders set quiqupJobID=?, pickupState=?, dropOffstate=?, pickupTrackingURL=?, dropOffTrackingURL=? where order_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//DELETE A BANNER
router.delete("/:id", (req, res) => {
  const order_id = req.params.id;
  const data = [order_id];
  var sql = "delete from orders where order_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});
module.exports = router;
