const express = require("express");
const mysql = require("mysql2");
const router = express.Router();
require("dotenv").config();
var con = require("../db");

//Get
router.get("/", (req, res) => {
  var sql = "select * from product_relations";
  con.query(sql, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//Get by ID
router.get("/:id", (req, res) => {
  const sql = "select * from product_relations where product_relation_id = ?";
  con.query(sql, [req.params.id], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});

//POST - ADD A PRODUCT RELATION
router.post("/", (req, res) => {
  const shoe_id = req.body.shoe_id;
  const related_product_id = req.body.related_product_id;
  const data = [shoe_id, related_product_id];
  const sql =
    "INSERT INTO product_relations (shoe_id, related_product_id) values(?,?)";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//PUT - UPDATE A PRODUCT RELATION
router.put("/:id", (req, res) => {
  const shoe_id = req.body.shoe_id;
  const related_product_id = req.body.related_product_id;
  const product_relation_id = req.params.id;
  const data = [shoe_id, related_product_id, product_relation_id];
  const sql =
    "update product_relations set shoe_id=?,related_product_id=? where product_relation_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//DELETE A PRODUCT RELATION
router.delete("/:id", (req, res) => {
  const product_relation_id = req.params.id;
  const data = [product_relation_id];
  var sql = "delete from product_relations where product_relation_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
module.exports = router;
