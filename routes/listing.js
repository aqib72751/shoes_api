const express = require("express");
const mysql = require("mysql2");
const router = express.Router();
require("dotenv").config();
var con = require("../db");

//Get
router.get("/", (req, res) => {
  var sql =
    "select * from listing as l join shoes as s on l.shoe_id = s.shoe_id";
  con.query(sql, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//Get HIGHEST ASK
router.get("/getHighestAsk", (req, res) => {
  const sql = "select max(askingPrice) from listing";
  con.query(sql, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});
//Get by ID
router.get("/:id", (req, res) => {
  const sql = "select * from listing where listing_id = ?";
  con.query(sql, [req.params.id], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});

//Get by sellerID
router.get("/getBySellerID/:seller_id", (req, res) => {
  const seller_id = req.params.seller_id;
  const sql = "select * from listing where seller_id = ?";
  con.query(sql, [seller_id], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//Get by shoeID and size
router.get("/getByShoeIDAndSize/:shoe_id/:size", (req, res) => {
  const shoe_id = req.params.shoe_id;
  const size = req.params.size;
  const date = new Date();
  const sql =
    "select * from listing where shoe_id=? and size=? and status=? and validTill>=? order by askingPrice asc, postedOn desc";
  con.query(sql, [shoe_id, size, "available", date], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      if (result.length == 0) {
        console.log("shoe not found");
        res.send("shoe not found");
      } else {
        console.log(result[0]);
        res.send(result[0]);
      }
    }
  });
});
//Get by sellerID and status
router.get("/getBySellerIDAndStatus/:seller_id/:status", (req, res) => {
  const seller_id = req.params.seller_id;
  const status = req.params.status;
  const sql = "select * from listing where seller_id=? and status=?";
  con.query(sql, [seller_id, status], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

//Get HIGHEST ASK BY SHOE ID
router.get("/getHighestAskByShoeID/:shoe_id", (req, res) => {
  const sql = "select max(askingPrice) from listing where shoe_id = ?";
  con.query(sql, [req.params.shoe_id], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});
//Get HIGHEST ASK BY SHOE ID AND SIZE
router.get("/getHighestAskByShoeIDAndSize/:shoe_id/:size", (req, res) => {
  const sql =
    "select max(askingPrice) from listing where shoe_id = ? and size=?";
  con.query(sql, [req.params.shoe_id, req.params.size], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});
//Get LOWEST ASK BY SHOE ID AND SIZE
router.get("/getLowestestAskByShoeIDAndSize/:shoe_id/:size", (req, res) => {
  const sql =
    "select min(askingPrice) from listing where shoe_id = ? and size=?";
  con.query(sql, [req.params.shoe_id, req.params.size], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});
router.post("/", (req, res) => {
  const seller_id = req.body.seller_id;
  const shoe_id = req.body.shoe_id;
  const askingPrice = req.body.askingPrice;
  const size = req.body.size;
  const postedOn = req.body.postedOn;
  const validTill = req.body.validTill;
  const expiry = req.body.expiry;
  const isAuthentic = req.body.isAuthentic;
  const vat = req.body.vat;
  const processingFee = req.body.processingFee;
  const shippingFee = req.body.shippingFee;
  const payout = req.body.payout;
  const status = req.body.status;
  const soldTo = req.body.soldTo;
  const soldDate = req.body.soldDate;
  const acceptedOffer_id = req.body.acceptedOffer_id;
  const isReturnable = req.body.isReturnable;
  const data = [
    seller_id,
    shoe_id,
    askingPrice,
    size,
    postedOn,
    validTill,
    expiry,
    isAuthentic,
    vat,
    processingFee,
    shippingFee,
    payout,
    status,
    soldTo,
    soldDate,
    acceptedOffer_id,
    isReturnable,
  ];
  const sql =
    "insert into listing (seller_id,shoe_id,askingPrice,size,postedOn,validTill,expiry,isAuthentic,vat,processingFee,shippingFee,payout,status,soldTo,soldDate,acceptedOffer_id,isReturnable) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

router.put("/:id", (req, res) => {
  const seller_id = req.body.seller_id;
  const shoe_id = req.body.shoe_id;
  const askingPrice = req.body.askingPrice;
  const size = req.body.size;
  const postedOn = req.body.postedOn;
  const validTill = req.body.validTill;
  const expiry = req.body.expiry;
  const isAuthentic = req.body.isAuthentic;
  const vat = req.body.vat;
  const processingFee = req.body.processingFee;
  const shippingFee = req.body.shippingFee;
  const payout = req.body.payout;
  const status = req.body.status;
  const soldTo = req.body.soldTo;
  const soldDate = req.body.soldDate;
  const acceptedOffer_id = req.body.acceptedOffer_id;
  const listing_id = req.params.id;
  const isReturnable = req.body.isReturnable;
  const data = [
    seller_id,
    shoe_id,
    askingPrice,
    size,
    postedOn,
    validTill,
    expiry,
    isAuthentic,
    vat,
    processingFee,
    shippingFee,
    payout,
    status,
    soldTo,
    soldDate,
    acceptedOffer_id,
    listing_id,
    isReturnable,
  ];
  const sql =
    "update listing set seller_id=?,shoe_id=?,askingPrice=?,size=?,postedOn=?,validTill=?,expiry=?,isAuthentic=?,vat=?,processingFee=?,shippingFee=?,payout=?,status=?,soldTo=?,soldDate=?,acceptedOffer_id=?,isReturnable=? where listing_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//UPDATE
router.put("/updateAcceptedOfferIDByID/:id", (req, res) => {
  const id = req.params.id;
  const acceptedOffer_id = req.body.acceptedOffer_id;
  const sql = "update listing set acceptedOffer_id=?  where listing_id=?";
  con.query(sql, [acceptedOffer_id, id], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//UPDATE
router.delete("/:id", (req, res) => {
  const listing_id = req.params.id;
  const sql = "delete from listing  where listing_id=?";
  con.query(sql, [listing_id], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
module.exports = router;
