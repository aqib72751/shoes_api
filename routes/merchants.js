const express = require("express");
const router = express.Router();
const mysql = require("mysql2");
const con = require("../db.js");

router.get("/", (req, res) => {
  con.query("select * from merchants", (error, result) => {
    if (error) {
      res.send(error);
    } else {
      res.send(result);
    }
  });
});
router.get("/:merchant_id", (req, res) => {
  const { merchant_id } = req.params;
  const sql = "select * from merchants where merchant_id=?";
  con.query(sql, [merchant_id], (error, result) => {
    if (error) {
      res.send(error);
    } else {
      const ress = result[0];
      if (ress === null || ress === undefined || ress === "") {
        res
          .status(404)
          .json({ error: `record with the id ${merchant_id} not found` });
      } else {
        res.send(result[0]);
      }
    }
  });
});
router.post("/", (req, res) => {
  const { seller_id, merchant_account } = req.body;
  const sql = "insert into merchants (seller_id,merchant_account) values(?,?)";
  const data = [seller_id, merchant_account];
  con.query(sql, data, (error, result) => {
    if (error) {
      res.send(error);
    } else {
      res.send(result);
    }
  });
});
router.put("/:merchant_id", (req, res) => {
  const { merchant_id } = req.params;
  const sql = "select * from merchants where merchant_id=?";
  con.query(sql, [merchant_id], (error, result) => {
    if (error) {
      res.send(error);
    } else {
      const ress = result[0];
      if (ress === null || ress === undefined || ress === "") {
        res
          .status(404)
          .json({ error: `record with the id ${merchant_id} not found` });
      } else {
        const input = ({ seller_id, merchant_account } = req.body);
        const data = [seller_id, merchant_account, merchant_id];
        const sql =
          "update merchants set seller_id=?,merchant_account=? where merchant_id=?";
        con.query(sql, data, (error, result) => {
          if (error) {
            res.send(error);
          } else {
            res.send(result);
          }
        });
      }
    }
  });
});
router.delete("/:merchant_id", (req, res) => {
  const { merchant_id } = req.params;
  const sql = "select * from merchants where merchant_id=?";
  con.query(sql, [merchant_id], (error, result) => {
    if (error) {
      res.send(error);
    } else {
      const ress = result[0];
      if (ress === null || ress === undefined || ress === "") {
        res
          .status(404)
          .json({ error: `record with the id ${merchant_id} not found` });
      } else {
        const sql = "delete from merchants where merchant_id=?";
        con.query(sql, [merchant_id], (error, result) => {
          if (error) {
            res.send(error);
          } else {
            res.send(result);
          }
        });
      }
    }
  });
});
module.exports = router;
