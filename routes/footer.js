const express = require("express");
const mysql = require("mysql2");
const router = express.Router();
require("dotenv").config();
var con = require("../db");

//Get
router.get("/", (req, res) => {
  var sql = "select * from footer";
  con.query(sql, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//Get by ID
router.get("/:id", (req, res) => {
  const sql = "select * from footer where footer_id = ?";
  con.query(sql, [req.params.id], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});
//Get by columnNo
router.get("/getByColumnNo/:columnNo", (req, res) => {
  const sql = "select * from footer where columnNo = ? order by serialNo";
  con.query(sql, [req.params.columnNo], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});
//POST - ADD FOOTER
router.post("/", (req, res) => {
  const shoe_id = req.body.shoe_id;
  const title = req.body.title;
  const serialNo = req.body.serialNo;
  const data = [shoe_id, title, serialNo];
  var sql = "insert into footer (shoe_id, title, serialNo) values(?,?,?)";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//PUT - UPDATE FOOTER
router.put("/:id", (req, res) => {
  const shoe_id = req.body.shoe_id;
  const title = req.body.title;
  const footer_id = req.params.id;
  const serialNo = req.body.serialNo;
  const data = [shoe_id, title, serialNo, footer_id];
  var sql =
    "update footer set shoe_id=?, title=?, serialNo=? where footer_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//DELETE A FOOTER
router.delete("/:id", (req, res) => {
  const footer_id = req.params.id;
  const data = [footer_id];
  var sql = "delete from footer where footer_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
module.exports = router;
