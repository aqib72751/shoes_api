const express = require("express");
const mysql = require("mysql2");
const bcrypt = require("bcrypt");
const registrations = require("./routes/registrations");
const shoes = require("./routes/shoes");
const shoes_images = require("./routes/shoesImages");
const productRelations = require("./routes/productRelations");
const footer = require("./routes/footer");
const collections = require("./routes/collections");
const banners = require("./routes/banners");
const listing = require("./routes/listing");
const offers = require("./routes/offers");
const sms = require("./routes/sms");
const sellers = require("./routes/sellers");
const shippings = require("./routes/shippings");
const orders = require("./routes/orders");
const searches = require("./routes/searches");
const favourites = require("./routes/favourites");
const quiqup = require("./routes/quiqup");
const merchants = require("./routes/merchants");
const bodyParser = require("body-parser");
const axios = require("axios");
const { uuid } = require("uuidv4");

//Adyen
const { Client, Config, CheckoutAPI } = require("@adyen/api-library");
const config = new Config();
// Set your X-API-KEY with the API key from the Customer Area.
config.apiKey =
  "AQEhhmfuXNWTK0Qc+iSRgnQxpO+r8vYJAg78qL5xI0ARtZ/vEMFdWw2+5HzctViMSCJMYAc=-Nc+mduomD2n7EazbDDudyEuo9DMfv02utQJkNPJ/niY=-:Y~YJ%#K9[_FQw(:";
config.merchantAccount = "AppickECOM";
const client = new Client({ config });
client.setEnvironment("TEST");
const checkout = new CheckoutAPI(client);
// const paymentsResponse = checkout
//   .paymentMethods({
//     merchantAccount: config.merchantAccount,
//     countryCode: "NL",
//     shopperLocale: "nl-NL",
//     amount: { currency: "EUR", value: 1000 },
//     channel: "Web",
//   })
//   .then((res) => console.log(res));
// //
//   checkout
//   .payments({
//     merchantAccount: config.merchantAccount,
//     paymentMethod: {
//       type: "scheme",
//       encryptedCardNumber: "test_4111111111111111",
//       encryptedExpiryMonth: "test_03",
//       encryptedExpiryYear: "test_2030",
//       encryptedSecurityCode: "test_737",
//     },
//     amount: { currency: "EUR", value: 1000 },
//     reference: "YOUR_ORDER_NUMBER",
//     returnUrl: "https://your-company.com/checkout?shopperOrder=12xy..",
//   })
//   .then((res) => console.log(res));

require("dotenv").config();
const port = process.env.PORT || 3000;
//Middlewares
const app = express();
app.use(express.json());

const con = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  database: process.env.DB_NAME,
  password: process.env.DB_PASS,
});
con.connect((error, result) => {
  if (error) console.log(error);
  else {
    console.log("database connected");
    app.use("/registrations", registrations);
    app.use("/shoes", shoes);
    app.use("/shoesImages", shoes_images);
    app.use("/productRelations", productRelations);
    app.use("/footer", footer);
    app.use("/collections", collections);
    app.use("/banners", banners);
    app.use("/listing", listing);
    app.use("/offers", offers);
    app.use("/sms", sms);
    app.use("/sellers", sellers);
    app.use("/shippings", shippings);
    app.use("/orders", orders);
    app.use("/searches", searches);
    app.use("/favourites", favourites);
    app.use("/quiqup", quiqup);
    app.use("/merchants", merchants)
  }
});
app.get("/", (req, res) => {
  console.log("Thriller api running....");
  res.send("Thriller api running....");
});

// Get payment methods
app.post("/api/getPaymentMethods", async (req, res) => {
  try {
    const response = await checkout.paymentMethods({
      channel: "Web",
      merchantAccount: "AppickECOM",
    });
    res.json(response);
  } catch (err) {
    console.error(`Error: ${err.message}, error code: ${err.errorCode}`);
    res.status(err.statusCode).json(err.message);
  }
});
// Submitting a payment
app.post("/api/initiatePayment", async (req, res) => {
  const currency = findCurrency(req.body.paymentMethod.type);
  // find shopper IP from request
  const shopperIP =
    req.headers["x-forwarded-for"] || req.connection.remoteAddress;

  try {
    // unique ref for the transaction
    const orderRef = uuid();
    // Ideally the data passed here should be computed based on business logic
    const response = await checkout.payments({
      amount: { currency, value: 1000 }, // value is 10€ in minor units
      reference: orderRef, // required
      merchantAccount: "AppickECOM", // required
      channel: "Web", // required
      additionalData: {
        // required for 3ds2 native flow
        allow3DS2: true,
      },
      origin: "http://localhost:8080", // required for 3ds2 native flow
      browserInfo: req.body.browserInfo, // required for 3ds2
      shopperIP, // required by some issuers for 3ds2
      // we pass the orderRef in return URL to get paymentData during redirects
      returnUrl: `http://localhost:8080/api/handleShopperRedirect?orderRef=${orderRef}`, // required for 3ds2 redirect flow
      // special handling for boleto
      paymentMethod: req.body.paymentMethod.type.includes("boleto")
        ? {
            type: "boletobancario_santander",
          }
        : req.body.paymentMethod,
      // Below fields are required for Boleto:
      socialSecurityNumber: req.body.socialSecurityNumber,
      shopperName: req.body.shopperName,
      billingAddress:
        typeof req.body.billingAddress === "undefined" ||
        Object.keys(req.body.billingAddress).length === 0
          ? null
          : req.body.billingAddress,
      deliveryDate: "2023-12-31T23:00:00.000Z",
      shopperStatement:
        "Aceitar o pagamento até 15 dias após o vencimento.Não cobrar juros. Não aceitar o pagamento com cheque",
      // Below fields are required for Klarna:
      countryCode: req.body.paymentMethod.type.includes("klarna") ? "DE" : null,
      shopperReference: "12345",
      shopperEmail: "youremail@email.com",
      shopperLocale: "en_US",
      lineItems: [
        {
          quantity: "1",
          amountExcludingTax: "331",
          taxPercentage: "2100",
          description: "Shoes",
          id: "Item 1",
          taxAmount: "69",
          amountIncludingTax: "400",
        },
        {
          quantity: "2",
          amountExcludingTax: "248",
          taxPercentage: "2100",
          description: "Socks",
          id: "Item 2",
          taxAmount: "52",
          amountIncludingTax: "300",
        },
      ],
    });

    const { action } = response;

    if (action) {
      paymentDataStore[orderRef] = action.paymentData;
    }
    res.json(response);
  } catch (err) {
    console.error(`Error: ${err.message}, error code: ${err.errorCode}`);
    res.status(err.statusCode).json(err.message);
  }
});

function findCurrency(type) {
  switch (type) {
    case "ach":
      return "USD";
    case "wechatpayqr":
    case "alipay":
      return "CNY";
    case "dotpay":
      return "PLN";
    case "boletobancario":
    case "boletobancario_santander":
      return "BRL";
    default:
      return "EUR";
  }
}

app.listen(port, console.log("server runnning on port", port));
