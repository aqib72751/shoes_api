const express = require("express");
const mysql = require("mysql2");
const router = express.Router();
require("dotenv").config();
var con = require("../db");

//Get
router.get("/", (req, res) => {
  var sql = "select * from shoes_images";
  con.query(sql, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//Get by ID
router.get("/:id", (req, res) => {
  const sql = "select * from shoes_images where shoe_image_id = ?";
  con.query(sql, [req.params.id], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});
//POST - ADD SHOE IMAGE
router.post("/", (req, res) => {
  const shoe_id = req.body.shoe_id;
  const imgURL = req.body.imgURL;
  const data = [shoe_id, imgURL];
  var sql = "insert into shoes_images (shoe_id, imgURL) values(?,?)";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//PUT - UPDATE SHOE IMAGE
router.put("/:id", (req, res) => {
  const shoe_id = req.body.shoe_id;
  const imgURL = req.body.imgURL;
  const shoe_image_id = req.params.id;
  const data = [shoe_id, imgURL, shoe_image_id];
  var sql = "update shoes_images set shoe_id=?, imgURL=? where shoe_image_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//DELETE A SHOE IMAGE
router.delete("/:id", (req, res) => {
  const shoe_image_id = req.params.id;
  const data = [shoe_image_id];
  var sql = "delete from shoes_images where shoe_image_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
module.exports = router;
