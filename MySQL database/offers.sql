CREATE TABLE `offers` (
  `offer_id` int NOT NULL AUTO_INCREMENT,
  `buyer_id` int NOT NULL,
  `shoe_id` int NOT NULL,
  `size` varchar(255) DEFAULT NULL,
  `offerAmount` decimal(10,0) DEFAULT NULL,
  `vat` decimal(10,0) DEFAULT NULL,
  `processingFee` decimal(10,0) DEFAULT NULL,
  `shippingFee` decimal(10,0) DEFAULT NULL,
  `totalBill` decimal(10,0) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `acceptedOn` date DEFAULT NULL,
  PRIMARY KEY (`offer_id`),
  KEY `offers_ibfk_1` (`shoe_id`),
  CONSTRAINT `offers_ibfk_1` FOREIGN KEY (`shoe_id`) REFERENCES `shoes` (`shoe_id`)
)
