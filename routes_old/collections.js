const express = require("express");
const mysql = require("mysql2");
const router = express.Router();
require("dotenv").config();
var con = require("../db");

//Get
router.get("/", (req, res) => {
  var sql = "select * from collections";
  con.query(sql, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//Get by ID
router.get("/:id", (req, res) => {
  const sql = "select * from collections where collection_id = ?";
  con.query(sql, [req.params.id], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});
//POST - ADD COLLECTION
router.post("/", (req, res) => {
  const imageURL = req.body.imageURL;
  const title = req.body.title;
  const description = req.body.description;
  const data = [imageURL, title, description];
  var sql =
    "insert into collections (imageURL, title, description) values(?,?,?)";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//PUT - UPDATE COLLECTION
router.put("/:id", (req, res) => {
  const imageURL = req.body.imageURL;
  const title = req.body.title;
  const collection_id = req.params.id;
  const description = req.body.description;
  const data = [imageURL, title, description, collection_id];
  var sql =
    "update collections set imageURL=?, title=?, description=? where collection_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//DELETE A COLLECTION
router.delete("/:id", (req, res) => {
  const collection_id = req.params.id;
  const data = [collection_id];
  var sql = "delete from collections where collection_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
module.exports = router;
