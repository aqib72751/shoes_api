var mysql = require("mysql2");
require("dotenv").config();
var db;

function connectDatabase() {
  if (!db) {
    db = mysql.createPool({
      host: process.env.DB_HOST,
      user: process.env.DB_USER,
      database: process.env.DB_NAME,
      password: process.env.DB_PASS,
    });
db.getConnection((error, result)=>{
    if (error) {
            console.log(error)
        } else {
            console.log("Databse connected")
        }
})
  }
  return db;
}

module.exports = connectDatabase();
