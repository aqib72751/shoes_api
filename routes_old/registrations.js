const express = require("express");
const mysql = require("mysql2");
const router = express.Router();
require("dotenv").config();
var con = require("../db");

//Requests
//GET ALL
router.get("/", (req, res) => {
  con.query("select * from registrations", (err, result) => {
    if (err) {
      res.send(err.sqlMessage);
      console.log(err.sqlMessage);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//GET ONE BY ID
router.get("/:id", (req, res) => {
  con.query(
    "select * from registrations where user_id=?",
    [req.params.id],
    (err, result) => {
      if (err) res.send(err);
      else {
        const found = result.find((e) => e.user_id == req.params.id);
        if (!found) {
          console.log("not found");
          res.status(404).send("not found");
        } else {
          console.log(found[0]);
          res.send(found[0]);
        }
      }
    }
  );
});

//GET ONE BY Role
router.get("/role/:id", (req, res) => {
  con.query(
    "select * from registrations where user_role=?",
    [req.params.id],
    (err, result) => {
      if (err) res.send(err);
      else {
        res.send(result);
      }
    }
  );
});

//POST
router.post("/", (req, res) => {
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const email = req.body.email;
  const password = req.body.password;
  const isAuthenticated = req.body.isAuthenticated;
  const isApproved = req.body.isApproved;
  const isSuspended = req.body.isSuspended;
  const suspendedTill = req.body.suspendedTill;
  const defaultSize = req.body.defaultSize;
  const userInfo = req.body.userInfo;
  const user_role = req.body.user_role;
  const business_name = req.body.business_name;
  const contact = req.body.contact;
  const paypal = req.body.paypal;
  const address = req.body.address;

  var verification_code = Math.floor(100000 + Math.random() * 900000);
  verification_code = verification_code.toString().substring(0, 4);
  verification_code = parseInt(verification_code);
  const sql =
    "insert into registrations " +
    "(firstName,lastName, email, password,verification_code, isAuthenticated, isApproved, isSuspended,suspendedTill,defaultSize,userInfo, user_role, business_name, contact, paypal, address)" +
    " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
  const data = [
    firstName,
    lastName,
    email,
    password,
    verification_code,
    isAuthenticated,
    isApproved,
    isSuspended,
    suspendedTill,
    defaultSize,
    userInfo,
    user_role,
    business_name,
    contact,
    paypal,
    address,
  ];
  con.query(sql, data, (err, result) => {
    if (err) {
      res.send(err.sqlMessage);
      console.log(err.sqlMessage);
    } else {
      console.log(`Record inserted at id = ${result.insertId}`);
      res.send(`Record inserted at id = ${result.insertId}`);
    }
  });
});
//LOGIN AUTHENTICATION
router.post("/login", (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  con.query(
    "select * from registrations where email=? and password =?",
    [email, password],
    (err, result) => {
      if (err) res.send(err);
      else {
        const found = result.find((e) => e.email == email);
        if (!found) {
          console.log("incorrect email");
          res.status(404).send("incorrect email");
        } else {
          console.log(found);
          res.send(found);
        }
      }
    }
  );
});

//PUT - UPDATE A VALUE BY ID
router.put("/:id", (req, res) => {
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const email = req.body.email;
  const password = req.body.password;
  const isAuthenticated = req.body.isAuthenticated;
  const isApproved = req.body.isApproved;
  const isSuspended = req.body.isSuspended;
  const suspendedTill = req.body.suspendedTill;
  const defaultSize = req.body.defaultSize;
  const userInfo = req.body.userInfo;
  const user_role = req.body.user_role;
  const business_name = req.body.business_name;
  const contact = req.body.contact;
  const paypal = req.body.paypal;
  const address = req.body.address;

  var verification_code = Math.floor(100000 + Math.random() * 900000);
  verification_code = verification_code.toString().substring(0, 4);
  verification_code = parseInt(verification_code);
  con.query("select * from registrations", (err, result) => {
    if (err) res.send(err);
    else {
      const found = result.find((e) => e.user_id == req.params.id);
      if (!found) {
        console.log("not found");
        res.status(404).send("not found");
      } else {
        const sql =
          "update registrations set firstName=?, lastName=?, email=?, password =?," +
          " verification_code =?, isAuthenticated=?,  isApproved=?, isSuspended=?, suspendedTill=?, defaultSize=?, userInfo=?, user_role=?, business_name=?, contact=?, paypal=?, address=? where user_id=?";
        const data = [
          firstName,
          lastName,
          email,
          password,
          verification_code,
          isAuthenticated,
          isApproved,
          isSuspended,
          suspendedTill,
          defaultSize,
          userInfo,
          user_role,
          business_name,
          contact,
          paypal,
          address,
          req.params.id,
        ];

        con.query(sql, data, (err, result) => {
          if (err) {
            res.send(err.sqlMessage);
            console.log(err.sqlMessage);
          } else {
            console.log(`Record updated at id ${req.params.id}`);
            res.send(`Record updated at id ${req.params.id}`);
          }
        });
      }
    }
  });
});
//Delete
router.delete("/:id", (req, res) => {
  const sql = "delete from registrations where user_id=?";
  const data = [req.params.id];
  con.query("select * from registrations", (err, result) => {
    if (err) res.send(err);
    else {
      const found = result.find((e) => e.user_id == req.params.id);
      if (!found) {
        console.log("not found");
        res.status(404).send("not found");
      } else {
        const found = result[0].ResultFound;
        console.log("records fetched in put request");
        console.log(result[0].ResultFound);
        if (found == "notFound") {
          res.status(404).send("id not found");
        } else {
          con.query(sql, data, (err, result) => {
            if (err) {
              res.send(err.sqlMessage);
              console.log(err.sqlMessage);
            } else {
              console.log(`Record deleted for ${req.params.id}`);
              res.send(`Record deleted for ${req.params.id}`);
            }
          });
        }
      }
    }
  });
});
module.exports = router;
