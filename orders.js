const express = require("express");
const mysql = require("mysql2");
const router = express.Router();
require("dotenv").config();
var con = require("../db");

//Get
router.get("/", (req, res) => {
  var sql = "select * from orders";
  con.query(sql, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      //console.log(result);
      res.send(result);
    }
  });
});
//Get by ID
router.get("/:id", (req, res) => {
  const sql = "select * from orders where order_id = ?";
  con.query(sql, [req.params.id], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});
//POST - ADD order
router.post("/", (req, res) => {
  const currentDate = new Date().toISOString().slice(0, 19).replace("T", " ");

  console.log(req.body.offerData);
  const productID = req.body.offerData.productID;
  const size = req.body.offerData.size;
  const buyerID = req.body.offerData.buyerID;
  const sellerID = req.body.offerData.sellerID;
  const price = req.body.offerData.price;
  const isAuthentic = req.body.offerData.isAuthentic;
  const notes = req.body.offerData.notes;
  const status = req.body.offerData.status;
  const quiqupJobID = req.body.offerData.quiqupJobID;
  const pickupState = req.body.offerData.pickupState;
  const dropOffstate = req.body.offerData.dropOffstate;
  const pickupTrackingURL = req.body.offerData.pickupTrackingURL;
  const dropOffTrackingURL = req.body.offerData.dropOffTrackingURL;

  var offer_id = req.body.offer_id;
  var listing_id = req.body.listing_id;
  var soldTo = req.body.soldTo;

  const updateDate = [offer_id, listing_id, soldTo];

  const data = [
    productID,
    size,
    buyerID,
    sellerID,
    price,
    isAuthentic,
    notes,
    status,
    quiqupJobID,
    pickupState,
    dropOffstate,
    pickupTrackingURL,
    dropOffTrackingURL,
  ];
  var sql =
    "insert into orders (productID, size, buyerID, sellerID, price, isAuthentic, notes, status, quiqupJobID, pickupState, dropOffstate, pickupTrackingURL, dropOffTrackingURL) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send({
        status: "error",
        result: error,
      });
    } else {
      //Update listings....
      var sqlU = `update listing set status='sold', soldTo=${soldTo}, acceptedOffer_id=${offer_id}, soldDate='${currentDate}' where listing_id=${listing_id}`;
      con.query(sqlU, (error, result) => {
        if (error) {
          console.log(error);
        } else {
          //Update lowest ask
          const sqlLA =
            "select min(askingPrice) as lowest from listing where shoe_id = ? and size=? and status='available' and ValidTill >= ?";
          con.query(
            sqlLA,
            [
              productID,
              size,
              new Date().toISOString().slice(0, 19).replace("T", " "),
            ],
            (error, result) => {
              if (error) {
                console.log(error);
              } else {
                var lowest = result[0].lowest;
                console.log("L:", lowest);
                const sqlUU = `update offers set lowestAsk=${lowest} where shoe_id=${productID} and size=${size}`;
                con.query(sqlUU, (error, result) => {
                  if (error) {
                    console.log(error);
                  } else {
                    //console.log(result);
                  }
                });
              }
            }
          );
        }
      });

      //Update offers
      var sqlO = `update offers set status='Accepted' where offer_id=${offer_id}`;
      con.query(sqlO, (errorr, resultt) => {
        if (errorr) {
          console.log(errorr);
        } else {
          //Update Highest_available_offer
          const sqlHH =
            "select max(offerAmount) as highest from offers where shoe_id = ? and size = ? and status = 'Pending'";
          con.query(sqlHH, [productID, size], (errorrr, resulttt) => {
            if (errorrr) {
              console.log(errorrr);
            } else {
              var highest = resulttt[0].highest;
              console.log("H:", highest);

              const sqlFinal = `update listing set highest_offer=${highest} where shoe_id = ${productID} and size = ${size} and status='available' and ValidTill >= '${currentDate}'`;
              con.query(sqlFinal, (error, result) => {
                if (error) {
                  console.error(error);
                } else {
                  console.log(result);
                }
              });
            }
          });
        }
      });

      res.send({
        status: "success",
        result: result,
      });
    }
  });
});
//PUT - UPDATE ORDER
router.put("/:id", (req, res) => {
  const quiqupJobID = req.body.quiqupJobID;
  const pickupState = req.body.pickupState;
  const order_id = req.params.id;
  const dropOffstate = req.body.dropOffstate;
  const pickupTrackingURL = req.body.pickupTrackingURL;
  const dropOffTrackingURL = req.body.dropOffTrackingURL;
  const data = [
    quiqupJobID,
    pickupState,
    dropOffstate,
    pickupTrackingURL,
    dropOffTrackingURL,
    order_id,
  ];
  var sql =
    "update orders set quiqupJobID=?, pickupState=?, dropOffstate=?, pickupTrackingURL=?, dropOffTrackingURL=? where order_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      //console.log(result);
      res.send(result);
    }
  });
});
//DELETE A ORDER
router.delete("/:id", (req, res) => {
  const order_id = req.params.id;
  const data = [order_id];
  var sql = "delete from orders where order_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});
module.exports = router;
