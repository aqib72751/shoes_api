CREATE TABLE `shippings` (
  `shipping_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `address` text,
  `address2` text,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `zip` varchar(45) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`shipping_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `shippings_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `registrations` (`user_id`)
)
