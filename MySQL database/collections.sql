CREATE TABLE `collections` (
  `collection_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `imageURL` text,
  PRIMARY KEY (`collection_id`)
)