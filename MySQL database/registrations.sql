CREATE TABLE `registrations` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `verification_code` smallint DEFAULT NULL,
  `isAuthenticated` tinyint DEFAULT NULL,
  `isApproved` tinyint DEFAULT NULL,
  `isSuspended` tinyint DEFAULT NULL,
  `suspendedTill` date DEFAULT NULL,
  `defaultSize` varchar(45) DEFAULT NULL,
  `userInfo` text,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `idregisterations_UNIQUE` (`user_id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
)
