CREATE TABLE `footer` (
  `footer_id` int NOT NULL AUTO_INCREMENT,
  `shoe_id` int NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `serialNo` int NOT NULL,
  `columnNo` int DEFAULT NULL,
  `url` text,
  PRIMARY KEY (`footer_id`),
  KEY `footer_ibfk_1_idx` (`shoe_id`),
  CONSTRAINT `footer_ibfk_1` FOREIGN KEY (`shoe_id`) REFERENCES `shoes` (`shoe_id`)
)