const express = require("express");
const mysql = require("mysql2");
const router = express.Router();
require("dotenv").config();
var con = require("../db");

//Get
router.get("/", (req, res) => {
  var sql = "select * from offers";
  con.query(sql, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//Get by ID
router.get("/:id", (req, res) => {
  const sql = "select * from offers where offer_id = ?";
  con.query(sql, [req.params.id], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});
//Get by buyerID and status
router.get("/getByBuyerIDAndStatus/:buyer_id/:status", (req, res) => {
  const buyer_id = req.params.buyer_id;
  const status = req.params.status;
  const sql = "select * from offers where buyer_id = ? and status=?";
  con.query(sql, [buyer_id, status], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

//Get HIGHEST offer amount BY SHOE ID AND SIZE
router.get(
  "/getHighestOfferAmountByShoeIDAndSize/:shoe_id/:size",
  (req, res) => {
    const sql =
      "select max(offerAmount) from offers where shoe_id = ? and size=?";
    con.query(sql, [req.params.shoe_id, req.params.size], (error, result) => {
      if (error) {
        console.log(error);
        res.send(error);
      } else {
        console.log(result);
        res.send(result);
      }
    });
  }
);
//Get LOWEST offer amount BY SHOE ID AND SIZE
router.get(
  "/getLowestOfferAmountbyShoeIDAndSize/:shoe_id/:size",
  (req, res) => {
    const sql =
      "select min(offerAmount) from offers where shoe_id = ? and size=?";
    con.query(sql, [req.params.shoe_id, req.params.size], (error, result) => {
      if (error) {
        console.log(error);
        res.send(error);
      } else {
        console.log(result);
        res.send(result);
      }
    });
  }
);
router.post("/", (req, res) => {
  const buyer_id = req.body.buyer_id;
  const shoe_id = req.body.shoe_id;
  const size = req.body.size;
  const offerAmount = req.body.offerAmount;
  const vat = req.body.vat;
  const processingFee = req.body.processingFee;
  const shippingFee = req.body.shippingFee;
  const totalBill = req.body.totalBill;
  const status = req.body.status;
  const acceptedOn = req.body.acceptedOn;
  const data = [
    buyer_id,
    shoe_id,
    size,
    offerAmount,
    vat,
    processingFee,
    shippingFee,
    totalBill,
    status,
    acceptedOn,
  ];
  const sql =
    "insert into offers (buyer_id, shoe_id, size, offerAmount, vat, processingFee, shippingFee, totalBill, status, acceptedOn) values(?,?,?,?,?,?,?,?,?,?) ";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
router.put("/:id", (req, res) => {
  const buyer_id = req.body.buyer_id;
  const shoe_id = req.body.shoe_id;
  const size = req.body.size;
  const offerAmount = req.body.offerAmount;
  const vat = req.body.vat;
  const processingFee = req.body.processingFee;
  const shippingFee = req.body.shippingFee;
  const totalBill = req.body.totalBill;
  const status = req.body.status;
  const acceptedOn = req.body.acceptedOn;
  const offer_id = req.params.id;
  const data = [
    buyer_id,
    shoe_id,
    size,
    offerAmount,
    vat,
    processingFee,
    shippingFee,
    totalBill,
    status,
    acceptedOn,
    offer_id,
  ];
  const sql =
    "update offers set buyer_id=?, shoe_id=?, size=?, offerAmount=?, vat=?, processingFee=?, shippingFee=?, totalBill=?, status=?, acceptedOn=? where offer_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
router.delete("/:id", (req, res) => {
  const offer_id = req.params.id;
  const data = [offer_id];
  var sql = "delete from offers where offer_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
module.exports = router;
