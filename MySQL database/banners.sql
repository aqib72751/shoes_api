CREATE TABLE `banners` (
  `banner_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `imageURL` text,
  `buttonText` varchar(255) DEFAULT NULL,
  `buttonURL` text,
  PRIMARY KEY (`banner_id`)
)