const express = require("express");
const mysql = require("mysql2");
const router = express.Router();
require("dotenv").config();
var con = require("../db");

//Get
router.get("/", (req, res) => {
  var sql = "SELECT * FROM sms";
  con.query(sql, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//Get by ID
router.get("/:id", (req, res) => {
  const sql = "SELECT * FROM sms where sms_id = ?";
  con.query(sql, [req.params.id], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//Delete by ID
router.delete("/:id", (req, res) => {
  const sms_id = req.params.id;
  const data = [sms_id];
  const sql = "delete from sms where sms_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//POST - addSMS
router.post("/", (req, res) => {
  const destination = req.body.destination;
  const message = req.body.message;
  const origin = req.body.origin;
  const data = [destination, message, origin];
  var sql = "insert into sms (destination, message, origin) values(?,?,?)";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
module.exports = router;
