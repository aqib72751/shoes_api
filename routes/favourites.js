const express = require("express");
const mysql = require("mysql2");
const router = express.Router();
require("dotenv").config();
var con = require("../db");

//Get
router.get("/", (req, res) => {
  var sql =
    "SELECT * FROM favourites as f join shoes as s on f.shoe_id=s.shoe_id left join listing as l on f.shoe_id=l.shoe_id";
  con.query(sql, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      //console.log(result);
      res.send(result);
    }
  });
});
//Get by ID
router.get("/:id", (req, res) => {
  const sql =
    "SELECT * FROM favourites as f join shoes as s on f.shoe_id=s.shoe_id left join listing as l on f.shoe_id = l.shoe_id where f.user_id = ?";
  con.query(sql, [req.params.id], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      res.send(result);
    }
  });
});

//Get by shoe_id and user-id
router.get("/:shoe_id/:user_id", (req, res) => {
  const sql =
    "SELECT * FROM favourites as f join shoes as s on f.shoe_id=s.shoe_id left join listing as l on f.shoe_id = l.shoe_id where f.user_id = ? and f.shoe_id=?";
  con.query(sql, [req.params.user_id, req.params.shoe_id], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});

//POST - ADD FAVOURITES
router.post("/", (req, res) => {
  const shoe_id = req.body.shoe_id;
  const user_id = req.body.user_id;
  const data = [shoe_id, user_id];
  var sql = "select * from favourites";
  con.query(sql, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      const found = result.find(
        (e) => e.user_id == user_id && e.shoe_id == shoe_id
      );
      if (!found) {
        var sql = "insert into favourites (shoe_id, user_id) values(?,?)";
        con.query(sql, data, (error, result) => {
          if (error) {
            console.log(error);
            res.send(error);
          } else {
            //console.log(result);
            res.send(result);
          }
        });
      } else {
        console.log("already added");
        res.send("already added");
      }
    }
  });
});
//PUT - UPDATE FAVOURITES
router.put("/:id", (req, res) => {
  const shoe_id = req.body.shoe_id;
  const user_id = req.body.user_id;
  const favourite_id = req.params.id;
  const data = [shoe_id, user_id, favourite_id];
  var sql = "update favourites set shoe_id=?, user_id=? where favourite_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      //console.log(result);
      res.send(result);
    }
  });
});
//DELETE A FAVOURITES
router.delete("/:id", (req, res) => {
  const favourite_id = req.params.id;
  const data = [favourite_id];
  var sql = "delete from favourites where favourite_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      //console.log(result);
      res.send(result);
    }
  });
});
module.exports = router;
