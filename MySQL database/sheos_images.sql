CREATE TABLE `shoes_images` (
  `shoe_image_id` int NOT NULL,
  `shoe_id` int NOT NULL,
  `imgURL` text,
  PRIMARY KEY (`shoe_image_id`),
  KEY `shoe_id_idx` (`shoe_id`),
  CONSTRAINT `shoe_id` FOREIGN KEY (`shoe_id`) REFERENCES `shoes` (`shoe_id`)
)
