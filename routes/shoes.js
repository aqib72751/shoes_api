const express = require("express");
const mysql = require("mysql2");
const router = express.Router();
require("dotenv").config();
var con = require("../db");

//Get
router.get("/", (req, res) => {
  var sql = "SELECT * FROM shoes";
  con.query(sql, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//Get by ID
router.get("/:id", (req, res) => {
  const sql = "SELECT * FROM shoes where shoe_id = ?";
  con.query(sql, [req.params.id], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});
//Get by tag
router.get("/getByTag/:tag", (req, res) => {
  const tag = req.params.tag + "%";
  const sql = "SELECT * FROM shoes where tag LIKE ?";
  con.query(sql, [tag], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//Get by collection
router.get("/collections/:id", (req, res) => {
  const collectionID = req.params.id;
  const sql = "SELECT * FROM shoes where collection_id = ?";
  con.query(sql, [collectionID], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      //console.log(result);
      res.send(result);
    }
  });
});
//Get by name contains
router.get("/getByNameContain/:name", (req, res) => {
  const name = req.params.name + "%";
  const sql = "SELECT * FROM shoes where name LIKE ?";
  con.query(sql, [name], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//POST
router.post("/", (req, res) => {
  const sku_number = req.body.sku_number;
  const name = req.body.name;
  const summary = req.body.summary;
  const tag = req.body.tag;
  const colorway = req.body.colorway;
  const release_date = req.body.release_date;
  const highest_offer = req.body.highest_offer;
  const lowest_asking_price = req.body.lowest_asking_price;
  const cover_image = req.body.cover_image;
  const collection_id = req.body.collection_id;

  const data = [
    sku_number,
    name,
    summary,
    tag,
    colorway,
    release_date,
    highest_offer,
    lowest_asking_price,
    cover_image,
    collection_id,
  ];
  const sql =
    "insert into shoes (sku_number, name, summary, tag, colorway, release_date, highest_offer, lowest_asking_price, cover_image, collection_id) values(?,?,?,?,?,?,?,?,?,?)";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//UPDATE
router.put("/:id", (req, res) => {
  const sku_number = req.body.sku_number;
  const name = req.body.name;
  const summary = req.body.summary;
  const tag = req.body.tag;
  const colorway = req.body.colorway;
  const release_date = req.body.release_date;
  const highest_offer = req.body.highest_offer;
  const lowest_asking_price = req.body.lowest_asking_price;
  const cover_image = req.body.cover_image;
  const collection_id = req.body.collection_id;
  const shoe_id = req.params.id;
  const data = [
    sku_number,
    name,
    summary,
    tag,
    colorway,
    release_date,
    highest_offer,
    lowest_asking_price,
    cover_image,
    collection_id,
    shoe_id,
  ];
  const sql =
    "update shoes set " +
    "sku_number=?, name=?, summary=?, tag=?, colorway=?, release_date=?, highest_offer=?," +
    " lowest_asking_price=?, cover_image=?, collection_id=?" +
    " where shoe_id=?;";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//Delete by ID
router.delete("/:id", (req, res) => {
  const shoe_id = req.params.id;
  const data = [shoe_id];
  const sql = "delete from shoes where shoe_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

module.exports = router;
