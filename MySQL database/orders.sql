CREATE TABLE `orders` (
  `order_id` int NOT NULL AUTO_INCREMENT,
  `quiqupJobID` int DEFAULT NULL,
  `pickupState` varchar(255) DEFAULT NULL,
  `dropOffstate` varchar(255) DEFAULT NULL,
  `pickupTrackingURL` text,
  `dropOffTrackingURL` text,
  PRIMARY KEY (`order_id`)
)
