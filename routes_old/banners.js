const express = require("express");
const mysql = require("mysql2");
const router = express.Router();
require("dotenv").config();
var con = require("../db");

//Get
router.get("/", (req, res) => {
  var sql = "select * from banners";
  con.query(sql, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//Get by ID
router.get("/:id", (req, res) => {
  const sql = "select * from banners where banner_id = ?";
  con.query(sql, [req.params.id], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});
//POST - ADD BANNER
router.post("/", (req, res) => {
  const imageURL = req.body.imageURL;
  const title = req.body.title;
  const description = req.body.description;
  const buttonText = req.body.buttonText;
  const buttonURL = req.body.buttonURL;
  const data = [imageURL, title, description, buttonText, buttonURL];
  var sql =
    "insert into banners (imageURL, title, description, buttonText, buttonURL) values(?,?,?,?,?)";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//PUT - UPDATE BANNER
router.put("/:id", (req, res) => {
  const imageURL = req.body.imageURL;
  const title = req.body.title;
  const banner_id = req.params.id;
  const description = req.body.description;
  const buttonText = req.body.buttonText;
  const buttonURL = req.body.buttonURL;
  const data = [imageURL, title, description, buttonText, buttonURL, banner_id];
  var sql =
    "update banners set imageURL=?, title=?, description=?, buttonText=?, buttonURL=? where banner_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//DELETE A BANNER
router.delete("/:id", (req, res) => {
  const banner_id = req.params.id;
  const data = [banner_id];
  var sql = "delete from banners where banner_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
module.exports = router;
