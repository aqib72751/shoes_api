CREATE TABLE `shoes` (
  `shoe_id` int NOT NULL AUTO_INCREMENT,
  `sku_number` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `summary` text,
  `tag` varchar(255) DEFAULT NULL,
  `colorway` varchar(255) DEFAULT NULL,
  `release_date` date DEFAULT NULL,
  `highest_offer` decimal(10,0) DEFAULT NULL,
  `lowest_asking_price` decimal(10,0) DEFAULT NULL,
  `cover_image` text,
  `collection_id` int NOT NULL,
  PRIMARY KEY (`shoe_id`),
  KEY `collection_id` (`collection_id`),
  CONSTRAINT `shoes_ibfk_1` FOREIGN KEY (`collection_id`) REFERENCES `collections` (`collection_id`)
)