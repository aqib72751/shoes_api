CREATE TABLE `product_relations` (
  `product_relation_id` int NOT NULL AUTO_INCREMENT,
  `shoe_id` int NOT NULL,
  `related_product_id` int NOT NULL,
  PRIMARY KEY (`product_relation_id`),
  KEY `shoe_id` (`shoe_id`),
  KEY `related_product_id` (`related_product_id`),
  CONSTRAINT `product_relations_ibfk_1` FOREIGN KEY (`shoe_id`) REFERENCES `shoes` (`shoe_id`),
  CONSTRAINT `product_relations_ibfk_2` FOREIGN KEY (`related_product_id`) REFERENCES `shoes` (`shoe_id`)
)
