const express = require("express");
const mysql = require("mysql2");
const router = express.Router();
require("dotenv").config();
var con = require("../db");

//Get
router.get("/", (req, res) => {
  var sql = "select * from sellers";
  con.query(sql, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//Get by ID
router.get("/:id", (req, res) => {
  const sql = "select * from sellers where seller_id = ?";
  con.query(sql, [req.params.id], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});
//POST - ADD seller
router.post("/", (req, res) => {
  const user_id = req.body.user_id;
  const email = req.body.email;
  const country = req.body.country;
  const address = req.body.address;
  const address2 = req.body.address2;
  const city = req.body.city;
  const state = req.body.state;
  const zip = req.body.zip;
  const phone = req.body.phone;
  const card = req.body.card;
  const cctv = req.body.cctv;
  const sameShipping = req.body.sameShipping;
  const data = [
    user_id,
    email,
    country,
    address,
    address2,
    city,
    state,
    zip,
    phone,
    card,
    cctv,
    sameShipping,
  ];
  var sql =
    "insert into sellers (user_id,email,country,address,address2,city,state,zip,phone,card,cctv,sameShipping) values(?,?,?,?,?,?,?,?,?,?,?,?)";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//PUT - UPDATE SELLER
router.put("/:id", (req, res) => {
  const user_id = req.body.user_id;
  const email = req.body.email;
  const country = req.body.country;
  const address = req.body.address;
  const address2 = req.body.address2;
  const city = req.body.city;
  const state = req.body.state;
  const zip = req.body.zip;
  const phone = req.body.phone;
  const card = req.body.card;
  const cctv = req.body.cctv;
  const sameShipping = req.body.sameShipping;
  const data = [
    user_id,
    email,
    country,
    address,
    address2,
    city,
    state,
    zip,
    phone,
    card,
    cctv,
    sameShipping,
    req.params.id,
  ];
  var sql =
    "update sellers set user_id=?, email=?, country=?, address=?, address2=?, city=?, state=?, zip=?, phone=?, card=?, cctv=?, sameShipping=? where seller_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//DELETE A seller
router.delete("/:id", (req, res) => {
  const seller_id = req.params.id;
  const data = [seller_id];
  var sql = "delete from sellers where seller_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
module.exports = router;
