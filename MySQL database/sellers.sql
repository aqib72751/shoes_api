CREATE TABLE `sellers` (
  `seller_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `address` text,
  `address2` text,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `zip` varchar(45) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `card` varchar(20) DEFAULT NULL,
  `cctv` int DEFAULT NULL,
  `sameShipping` tinyint DEFAULT NULL,
  PRIMARY KEY (`seller_id`),
  KEY `sellers_ibfk_1` (`user_id`),
  CONSTRAINT `sellers_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `registrations` (`user_id`)
)
