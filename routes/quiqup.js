const express = require("express");
const router = express.Router();
require("dotenv").config();
var con = require("../db");
const axios = require("axios");
//CHECK
router.get("/", (req, res) => {
  res.send("quiqup is running");
});
var auth = "";
var URL = "https://api-ae.quiqup.com/oauth/token";
//AUTH
router.post("/", (req, res) => {
  var grant_type = "client_credentials";
  const data = ({ grant_type, client_id, client_secret } = {
    grant_type,
    ...req.body,
  });
  let url = "https://api-ae.quiqup.com/oauth/token";
  //let url2 = `https://api-ae.quiqup.com/oauth/token?grant_type=${grant_type}&client_id=${client_id}&client_secret=${client_secret}`;
  //res.send(data)
  axios
    .post(url, {}, { params: data })
    .then((response) => {
      const token = ({
        access_token,
        token_type,
        expires_in,
        created_at,
      } = response.data);
      auth = `${token_type} ${access_token}`;
      res.send(token);
    })
    .catch((error) => res.send(error));
});
//OnDemand JOB CREATE
router.post("/createOnDemandJob", (req, res) => {
  let bodyData = ({
    needs_return,
    transport_mode,
    pickups: [
      {
        contact_name: contact_name_pickup,
        contact_phone: contact_phone_pickup,
        partner_order_id: partner_order_id_pickup,
        notes: notes_pickup,
        location: {
          address1: address1_pickup,
          address2: address2_pickup,
          coords: [latitude_pickup, longitude_pickup],
          country: country_pickup,
          town: town_pickup,
        },
        items: [{ name, quantity }],
      },
    ],
    dropoffs: [
      {
        contact_name: contact_name_dropoff,
        contact_phone: contact_phone_dropoff,
        payment_mode: payment_mode_dropoff,
        payment_amount: payment_amount_dropoff,
        location: {
          address1: address1_dropoff,
          address2: address2_dropoff,
          coords: [latitude_dropoff, longitude_dropoff],
          country: country_dropoff,
          town: town_dropoff,
        },
      },
    ],
  } = req.body);
  var order_id = parseInt(partner_order_id_pickup);
  partner_order_id_pickup = "S-" + partner_order_id_pickup;
  const url = "https://api-ae.quiqup.com/partner/jobs";
  axios
    .post(url, bodyData, { headers: { Authorization: auth } })
    .then((response) => {
      const sql = "select * from orders where order_id=?";
      con.query(sql, [order_id], (error, result) => {
        if (error) return res.status(400).json(error);
        else {
          let ress = result[0];
          if (ress !== null || ress !== "" || ress !== undefined) {
            const sql =
              "update orders set quiqupJobID=?,pickupTrackingURL=?, dropOffTrackingURL=? where order_id=?";
            const data = [
              response.data.orders[0].id,
              response.data.orders[0].pickup.tracking_url,
              response.data.orders[0].dropoff.tracking_url,
              order_id,
            ];
            con.query(sql, data, (error, result) => {
              if (error) return res.status(400).json(error);
              else return res.status(200).json(response.data);
            });
          } else return res.status(404).json("order id not found");
        }
      });
    })
    .catch((error) => res.status(400).json(error));
});
router.post("/listOnDemandJob", (req, res) => {
  const url = "https://api-ae.quiqup.com/partner/jobs";
  const parameters = ({
    current,
    current_page,
    per_page,
    include,
    timeframe,
  } = req.body);
  axios
    .get(url, { params: parameters, headers: { Authorization: auth } })
    .then((response) => res.send(response.data))
    .catch((error) => res.send(error));
});
router.post("/submitOnDemandJob", (req, res) => {
  const { job_id } = req.body;
  console.log(auth);
  const url = `https://api-ae.quiqup.com/partner/jobs/${job_id}/submissions`;
  axios
    .post(url, {}, { headers: { Authorization: auth } })
    .then((response) => res.send(response.data))
    .catch((error) => res.send(error));
});
router.get("/getOnDemandJob/:job_id", (req, res) => {
  const { job_id } = req.params;
  const url = `https://api-ae.quiqup.com/partner/jobs/${job_id}`;
  axios
    .get(url, { headers: { Authorization: auth } })
    .then((response) => res.send(response.data))
    .catch((error) => res.send(error));
});
// router.put("/updateOnDemandJob/:job_id", (req, res) => {
//   const { job_id } = req.params;
//   const { scheduled_for }= req.body;
//   const url = `https://api-ae.quiqup.com/partner/jobs/${job_id}`;
//   axios
//     .put(url,{scheduled_for}, { headers: { Authorization: auth } })
//     .then((response) => res.send(response.data))
//     .catch((error) => res.send(error));
// });
module.exports = router;
