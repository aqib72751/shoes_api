const express = require("express");
const mysql = require("mysql2");
const router = express.Router();
require("dotenv").config();
var con = require("../db");

//Get
router.get("/", (req, res) => {
  var sql = "select * from shippings";
  con.query(sql, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      //console.log(result);
      res.send(result);
    }
  });
});
//Get by ID
router.get("/:id", (req, res) => {
  const sql = "select * from shippings where user_id = ?";
  con.query(sql, [req.params.id], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result[0]);
      res.send(result[0]);
    }
  });
});
//POST - ADD shipping
router.post("/", (req, res) => {
  const user_id = req.body.user_id;
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const country = req.body.country;
  const address = req.body.address;
  const address2 = req.body.address2;
  const city = req.body.city;
  const state = req.body.state;
  const zip = req.body.zip;
  const phone = req.body.phone;
  const data = [
    user_id,
    firstName,
    lastName,
    country,
    address,
    address2,
    city,
    state,
    zip,
    phone,
  ];

  var sqlD = "delete from shippings where user_id=?";
  con.query(sqlD, user_id, (err, ress) => {
    if (err) {
      console.log(err);      
    } else {
      var sql =
      "insert into shippings (user_id,firstName,lastName,country,address,address2,city,state,zip,phone) values(?,?,?,?,?,?,?,?,?,?)";
      con.query(sql, data, (error, result) => {
        if (error) {
          console.log(error);
          res.send(error);
        } else {
          //console.log(result);
          res.send(result);
        }
      });
    }
  });  
});


//PUT - UPDATE shipping
router.put("/:id", (req, res) => {
  const user_id = req.body.user_id;
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const country = req.body.country;
  const address = req.body.address;
  const address2 = req.body.address2;
  const city = req.body.city;
  const state = req.body.state;
  const zip = req.body.zip;
  const phone = req.body.phone;
  const data = [
    user_id,
    firstName,
    lastName,
    country,
    address,
    address2,
    city,
    state,
    zip,
    phone,
    req.params.id,
  ];
  var sql =
    "update shippings set user_id=?, firstName=?, lastName=?, country=?, address=?, address2=?, city=?, state=?, zip=?, phone=? where shipping_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      //console.log(result);
      res.send(result);
    }
  });
});
//DELETE A shipping
router.delete("/:id", (req, res) => {
  const shipping_id = req.params.id;
  const data = [shipping_id];
  var sql = "delete from shippings where shipping_id=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      //console.log(result);
      res.send(result);
    }
  });
});
module.exports = router;
