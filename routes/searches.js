const express = require("express");
const mysql = require("mysql2");
const router = express.Router();
require("dotenv").config();
var con = require("../db");

//GET POPULAR SEARCHES
router.get("/popular/:limit", (req, res) => {
  const limit = parseInt(req.params.limit);
  var sql = "select * from popular order by count desc limit ?";
  con.query(sql, [limit], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
//GET RECENT SEARCHES
router.get("/recent/:id/:limit", (req, res) => {
  const user_id = req.params.id;
  const limit = parseInt(req.params.limit);
  var sql =
    "select * from recent where user_id=? order by dateTime desc limit ?";
  con.query(sql, [user_id, limit], (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

//POST - ADD SEARCHES
router.post("/", (req, res) => {
  const keyword = req.body.keyword;
  const user_id = req.body.user_id;
  let d = new Date();
  let dateTimeNow =
    d.toISOString().substr(0, 10) + " " + d.toISOString().substr(12, 8);
  const data = [dateTimeNow, user_id, keyword];
  //Recent SEARCH
  const sql = "select * from recent where user_id = ? and keyword =?";
  con.query(sql, [user_id, keyword], (error, result) => {
    if (error) {
      res.send(error);
    } else {
      var ress = result[0];
      if (ress === null || ress === "" || ress === undefined) {
        const sql =
          "insert into recent (dateTime, user_id, keyword) values(?,?,?)";
        con.query(sql, data, (error, result) => {
          if (error) res.send(error);
        });
      } else {
        const sql =
          "update recent set dateTime=? where user_id=? and keyword=?";
        con.query(sql, data, (error, result) => {
          if (error) res.send(error);
        });
      }
    }
  });
  //POPULAR SEARCH
  const sql2 = "select * from popular where keyword = ?";
  con.query(sql2, [keyword], (error, result) => {
    if (error) {
      res.send(error);
    } else {
      var ress = result[0];
      if (ress === null || ress === "" || ress === undefined) {
        const sql = "insert into popular (keyword) values(?)";
        con.query(sql, [keyword], (error, result) => {
          if (error) res.send(error);
        });
      } else {
        const sql = "update popular set count=count+1";
        con.query(sql, (error, result) => {
          if (error) res.send(error);
        });
      }
    }
  });
  res.send("keyword added successfuly");
});

module.exports = router;
